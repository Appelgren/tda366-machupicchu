package mvc.gui.impl.thumbnailspanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import mvc.core.IMachuPicchu;
import mvc.image.MachuImage;

public class ThumbnailsPanelPresenter implements PropertyChangeListener,
		ActionListener, KeyListener {
	private ThumbnailsPanel panel;
	private IMachuPicchu machu;
	private List<ThumbnailButton> buttonList = new ArrayList<ThumbnailButton>();

	public ThumbnailsPanelPresenter(IMachuPicchu machuPicchu,
			ThumbnailsPanel thumbnail) {
		;
		this.machu = machuPicchu;
		machu.addPropertyChangeListener(this);
		this.panel = thumbnail;

	}

	/*
	 * listens to Model. Updates Thumbnails when changed has occured. Also sets
	 * an image as selected a default image based on which is first in
	 * imageList. (non-Javadoc)
	 * 
	 * @seejava.beans.PropertyChangeListener#propertyChange(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("imageListChanged")) { //$NON-NLS-1$
			updateThumbnails();

		} else if (evt.getPropertyName().equals("selectedImagesChanged")) { //$NON-NLS-1$
			updateSelection();
		}

	}

	/**
	 * Updates the selected ThumbnailsButtons.
	 */
	private void updateSelection() {
		for (ThumbnailButton b : buttonList) {
			boolean found = false;
			for (MachuImage img : machu.getSelectedImages()) {
				if (b.getMachuImage() == img) {
					b.setSelected();
					found = true;

				} else if (!found) {
					b.setUnselected();
				}
			}
		}
	}

	/**
	 * Updates the thumbails by creating new thumbnails and adding them to the
	 * panel.
	 */
	private void updateThumbnails() {
		if (machu.getImageList().size() > 0) {
			panel.getThumbnailsPanel().removeAll();
			for (MachuImage i : machu.getImageList()) {
				ThumbnailButton b = new ThumbnailButton(new ImageIcon(i
						.getThumbnail()), i);
				b.addActionListener(this);
				b.addKeyListener(this);
				panel.getThumbnailsPanel().add(b);
				buttonList.add(b);

			}
		} else {
			panel.getThumbnailsPanel().removeAll();
			buttonList.clear();
		}
		updateSelection();
		panel.validate();
	}

	/*
	 * Listens on when a button is pressed to update which thumbnail to be
	 * selected.
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		ThumbnailButton button;

		if (e.getSource() instanceof ThumbnailButton) {
			button = ((ThumbnailButton) e.getSource());

			if ((e.getModifiers() & ActionEvent.SHIFT_MASK) == 1) {
				int startIndex = machu.getImageList().indexOf(
						machu.getSelectedImages().get(0));
				int endIndex = machu.getImageList().indexOf(
						button.getMachuImage());

				if (startIndex < endIndex) {
					machu.setSelectedImages(new ArrayList<MachuImage>(machu
							.getImageList().subList(startIndex, endIndex + 1)));
					// +1 to include endIndex in selection
				} else {
					machu.setSelectedImages(new ArrayList<MachuImage>(machu
							.getImageList().subList(endIndex, startIndex + 1)));
					// +1 to include startIndes in selection
				}

			} else if ((e.getModifiers() & ActionEvent.CTRL_MASK) == 2) {
				machu.addToSelectedImages(button.getMachuImage());
			} else {
				List<MachuImage> list = new ArrayList<MachuImage>();
				list.add(button.getMachuImage());

				machu.setSelectedImages(machu.testImages(list));
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

		int keyCode = e.getKeyCode();
		int index = machu.getImageList().indexOf(
				machu.getSelectedImages().get(0));

		switch (keyCode) {

		case KeyEvent.VK_RIGHT:// Right arrow key pressed
			if (index != machu.getImageList().size() - 1) {
				machu.setSelectedImages(machu.getImageList().subList(index + 1,
						index + 2)); // Ugly method call but this is to get next
				// image in the imagelist.
			}
			break;

		case KeyEvent.VK_LEFT:// Left arrow key pressed
			if (index != 0) {
				machu.setSelectedImages(machu.getImageList().subList(index - 1,
						index)); // Ugly method call but this is to get next
				// image in the imagelist.
			}
			break;

		case KeyEvent.VK_DELETE:
			machu.removeImage(machu.getSelectedImages());
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

}
