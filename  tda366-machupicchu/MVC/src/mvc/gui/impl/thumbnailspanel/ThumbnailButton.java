package mvc.gui.impl.thumbnailspanel;

import java.awt.Color;

import javax.swing.Icon;

import mvc.image.MachuImage;

/**
 * 
 * @author Berget
 */
@SuppressWarnings("serial")
public class ThumbnailButton extends javax.swing.JButton {

	private boolean selected = false;
	private transient MachuImage machuImage;

	/**
	 * Creates new form ThumbnailButton
	 */
	public ThumbnailButton(Icon i, MachuImage image) {
		this.machuImage = image;
		initComponents();
		this.setIcon(i);
	}

	/**
	 * sets the thumbnail as selected.
	 */
	public void setSelected() {
		this.setBorder(javax.swing.BorderFactory.createLineBorder(
				new java.awt.Color(51, 255, 51), 2));
		selected = true;
	}

	/**
	 * unselects the thumbnail.
	 */
	public void setUnselected() {
		this.setBorder(javax.swing.BorderFactory.createLineBorder(
				new java.awt.Color(0, 0, 0), 2));
		selected = false;
	}

	/*
	 * returns if the button is selected boolean. (non-Javadoc)
	 * 
	 * @see javax.swing.AbstractButton#isSelected()
	 */
	@Override
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Returns the MachuImage related to Button
	 * 
	 * @return MachuImage
	 */
	public MachuImage getMachuImage() {
		return machuImage;
	}

	private void initComponents() {
		this.setOpaque(false);
		this.setBackground(new java.awt.Color(0, 0, 0));
		this.setContentAreaFilled(false);
		this.setToolTipText(machuImage.getFile().getName());
		this.setBorder(javax.swing.BorderFactory.createLineBorder(Color.BLACK,
				2));
	}
}