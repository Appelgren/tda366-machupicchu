package mvc.gui.impl.thumbnailspanel;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import mvc.core.IMachuPicchu;

/**
 * 
 * @author Berget
 */
@SuppressWarnings("serial")
public class ThumbnailsPanel extends javax.swing.JPanel {

	private JPanel thumbnailsPanel;
	private javax.swing.JScrollPane scrollPanel;

	/**
	 * Creates new form ThumbnailsPanel2
	 */
	public ThumbnailsPanel(IMachuPicchu machuPicchu) {
		initComponents();
		new ThumbnailsPanelPresenter(machuPicchu, this);
	}

	private void initComponents() {

		scrollPanel = new javax.swing.JScrollPane();
		thumbnailsPanel = new javax.swing.JPanel();

		thumbnailsPanel.setBackground(new java.awt.Color(0, 0, 0));
		scrollPanel.setViewportView(thumbnailsPanel);

		scrollPanel.getHorizontalScrollBar().setUnitIncrement(60);
		scrollPanel.getHorizontalScrollBar().setBlockIncrement(180);
		scrollPanel.setBorder(new LineBorder(Color.DARK_GRAY, 3));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				scrollPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 801,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				scrollPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 213,
				Short.MAX_VALUE));
	}

	/**
	 * get method for thumbnailsPanel
	 * 
	 * @return thumbnailsPanel
	 */
	JPanel getThumbnailsPanel() {
		return thumbnailsPanel;
	}

}
