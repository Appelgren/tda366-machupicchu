package mvc.gui.impl.toolpanel;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingConstants;

import mvc.core.IMachuPicchu;
import mvc.effects.IEffect;

/**
 * 
 * @author Berget
 */
@SuppressWarnings("serial")
public final class EffectsButton extends javax.swing.JPanel {

	private javax.swing.JButton effectButton;
	private transient EffectsButtonPresenter presenter;
	private IEffect effect;

	/**
	 * Creates new form EffectsButton
	 */
	public EffectsButton(IMachuPicchu machu, IEffect effect) {
		this.presenter = new EffectsButtonPresenter(machu, this);
		this.effect = effect;

		initComponents();
	}

	private void initComponents() {

		this.setBackground(Color.BLACK);

		effectButton = new javax.swing.JButton();
		effectButton.setContentAreaFilled(false);
		effectButton.setText(effect.toString());
		effectButton.setForeground(Color.WHITE);
		effectButton.setBackground(Color.BLACK);
		effectButton.setBorderPainted(false);
		effectButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/mvc/gui/resources/effectButtonnew.png"))); //$NON-NLS-1$
		effectButton.setFont(new Font("Curier", 0, 14)); //$NON-NLS-1$
		effectButton.setHorizontalTextPosition(SwingConstants.CENTER);
		effectButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				applyEffectActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				effectButton, javax.swing.GroupLayout.DEFAULT_SIZE, 87,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				effectButton, javax.swing.GroupLayout.DEFAULT_SIZE, 82,
				Short.MAX_VALUE));
	}

	private void applyEffectActionPerformed(java.awt.event.ActionEvent evt) {
		presenter.applyEffect(effect);
	}

}
