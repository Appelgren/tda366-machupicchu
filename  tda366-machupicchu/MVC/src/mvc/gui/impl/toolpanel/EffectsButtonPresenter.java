package mvc.gui.impl.toolpanel;

import mvc.core.IMachuPicchu;
import mvc.effects.IEffect;

public class EffectsButtonPresenter {

	private IMachuPicchu machu;

	public EffectsButtonPresenter(IMachuPicchu machu, EffectsButton button) {

		this.machu = machu;

	}

	/*
	 * applies effect to selected image.
	 */
	void applyEffect(IEffect effect) {
		if (machu.getSelectedImages().size() > 0)
			machu.applyEffect(effect);
	}

}
