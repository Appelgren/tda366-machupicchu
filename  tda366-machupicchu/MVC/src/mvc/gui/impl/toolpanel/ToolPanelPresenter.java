package mvc.gui.impl.toolpanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import mvc.core.IMachuPicchu;
import mvc.effects.IEffect;

class ToolPanelPresenter implements PropertyChangeListener, ActionListener {
	private IMachuPicchu machu;
	private ToolPanel panel;
	private List<JButton> effectList;

	public ToolPanelPresenter(IMachuPicchu machuPicchu, ToolPanel toolpanel) {
		this.machu = machuPicchu;
		this.panel = toolpanel;
		machu.addPropertyChangeListener(this);
		addEffects();
		addEffectslist();

	}

	/*
	 * performs task to import images.
	 */
	void importPressed() {
		machu.importImages(150, 150);

	}

	/*
	 * performs task to export images.
	 */
	void exportPressed() {
		machu.exportImages();

	}

	/*
	 * adds the available effects to the toolPanel.
	 */
	private void addEffects() {

		List<IEffect> listWithEffects = machu.getListWithEffects();
		Iterator<IEffect> it = listWithEffects.iterator();
		EffectsButton b;

		while (it.hasNext()) {

			b = new EffectsButton(machu, it.next());

			panel.getEffectPanel().add(b);
		}
		panel.validate();

	}

	/**
	 * Adds the effectList linked to the image displayed on mainStage.
	 */
	private void addEffectslist() {
		// Gets the centerstageimage, retrives its effectlist and uses a
		// for-each loop
		// to add the effects to the effects list in GUI.
		if (machu.getSelectedImages().size() > 0) {

			panel.getEffectsListPanel().removeAll();

			effectList = new LinkedList<JButton>();

			for (IEffect fx : machu.getSelectedImages().get(0).getEffectsList()) {
				effectList.add(addEffectsListButton(fx));

			}
			setIndex(machu.getSelectedImages().get(0).getIndexOfCurrentEffect());
		}
		panel.validate();

	}

	/**
	 * Adds a Button to represent the Effect that has been applied to MainImage.
	 * 
	 * @param name
	 *            of Effect
	 */
	private JButton addEffectsListButton(IEffect fx) {

		JButton button = new JButton();
		button.setPreferredSize(new Dimension((int) panel.getEffectsListPanel()
				.getPreferredSize().getWidth(), 64));
		button.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/mvc/gui/resources/effectlistbut.png"))); //$NON-NLS-1$
		button.setBorderPainted(false);
		button.setBackground(Color.WHITE);
		button.setText(fx.toString());
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.setFont(new Font("Curier", 0, 14)); //$NON-NLS-1$
		button.setContentAreaFilled(false);
		button.addActionListener(this);

		panel.getEffectsListPanel().add(button);

		return button;
	}

	/**
	 * Will listen to Model. Make changes to effectslist linked to image
	 * displayed on mainStage. (non-Javadoc)
	 * 
	 * @seejava.beans.PropertyChangeListener#propertyChange(java.beans. 
	 *                                                                  PropertyChangeEvent
	 *                                                                  )
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("imageListChanged") || evt.getPropertyName().equals("selectedImagesChanged")) { //$NON-NLS-1$//$NON-NLS-2$
			addEffectslist();
			updateRemoveEffectsButton();
			updateExportButton();
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (effectList.contains(e.getSource())) {

			int index = effectList.indexOf(e.getSource());
			machu.getSelectedImages().get(0).setIndexOfCurrentEffect(index);
			machu.updateMainStageImage(150, 150);
			setIndex(index);

		}
	}

	/**
	 * sets the index of current effect in the effectList
	 * 
	 * @param index
	 */
	private void setIndex(int index) {
		for (int i = 0; i < effectList.size(); i++) {
			if (i <= index) {
				setEnabled(effectList.get(i));
			} else {
				setDisabled(effectList.get(i));
			}
		}
		panel.validate();

	}

	/**
	 * sets effectButton in effectList disabled
	 * 
	 * @param button
	 */
	private void setDisabled(JButton button) {
		button.setForeground(Color.BLACK);
		button.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/mvc/gui/resources/effectlistbutdeselected.png"))); //$NON-NLS-1$
	}

	/**
	 * sets effectButton in effectList enabled
	 * 
	 * @param button
	 */
	private void setEnabled(JButton button) {
		button.setForeground(Color.WHITE);
		button.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/mvc/gui/resources/effectlistbut.png"))); //$NON-NLS-1$
	}

	/**
	 * removes all effects in effectList
	 */
	void removeAllEffects() {
		if (machu.getSelectedImages().size() > 0) {

			machu.getSelectedImages().get(0).setEffectsList(
					new LinkedList<IEffect>());
			machu.getSelectedImages().get(0).setIndexOfCurrentEffect(-1);

			panel.getEffectsListPanel().removeAll();
			panel.validate();

			machu.updateMainStageImage(150, 150);

		}
	}

	private void updateRemoveEffectsButton() {
		if (machu.getSelectedImages().size() > 0) {
			if (machu.getSelectedImages().get(0).getIndexOfCurrentEffect() == -1) {
				panel.getRemoveAllEffectsButton().setEnabled(false);
			} else {
				panel.getRemoveAllEffectsButton().setEnabled(true);
			}
		}
	}

	private void updateExportButton() {
		if (machu.getSelectedImages().size() > 0) {
			panel.getExportButton().setEnabled(true);
		} else {
			panel.getExportButton().setEnabled(false);
		}
	}

}
