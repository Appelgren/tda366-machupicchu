package mvc.gui.impl.toolpanel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import mvc.core.IMachuPicchu;
import mvc.utils.MPResourceBundle;

/**
 * 
 * @author Berget
 */
@SuppressWarnings("serial")
public final class ToolPanel extends javax.swing.JPanel {

	private ResourceBundle messages;
	private transient ToolPanelPresenter presenter;

	private javax.swing.JPanel effectPanel;
	private javax.swing.JPanel effectsListPanel;
	private javax.swing.JButton exportButton;
	private javax.swing.JButton importButton;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JButton removeEffects;

	/**
	 * Creates new form ToolPanel2
	 */
	public ToolPanel(IMachuPicchu machuPicchu) {
		messages = MPResourceBundle.getResourceBundle();
		initComponents();
		presenter = new ToolPanelPresenter(machuPicchu, this);
	}

	private void initComponents() {

		importButton = new javax.swing.JButton();
		effectPanel = new javax.swing.JPanel();
		exportButton = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		effectsListPanel = new javax.swing.JPanel();
		removeEffects = new javax.swing.JButton();

		setBackground(java.awt.Color.black);
		setBorder(javax.swing.BorderFactory.createLineBorder(
				java.awt.Color.darkGray, 4));

		importButton.setBackground(new java.awt.Color(0, 0, 0));
		importButton.setForeground(new java.awt.Color(255, 255, 255));
		importButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/mvc/gui/resources/buttonIcon.png"))); //$NON-NLS-1$
		importButton.setText(messages.getString("import")); //$NON-NLS-1$
		importButton.setFont(new Font("Curier", 0, 14)); //$NON-NLS-1$
		importButton.setHorizontalTextPosition(SwingConstants.CENTER);
		importButton.setBorderPainted(false);
		importButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				importPressed(evt);
			}
		});

		effectPanel.setBackground(java.awt.Color.black);
		effectPanel.setBorder(javax.swing.BorderFactory.createLineBorder(
				java.awt.Color.darkGray, 4));
		effectPanel.setLayout(new java.awt.GridLayout(4, 2, 5, 5));

		exportButton.setForeground(new java.awt.Color(255, 255, 255));
		exportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/mvc/gui/resources/buttonIcon.png"))); //$NON-NLS-1$
		exportButton.setDisabledIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/mvc/gui/resources/buttonIcondisabled.png"))); //$NON-NLS-1$
		exportButton.setText(messages.getString("export")); //$NON-NLS-1$
		exportButton.setFont(new Font("Curier", 0, 14)); //$NON-NLS-1$
		exportButton.setHorizontalTextPosition(SwingConstants.CENTER);
		exportButton.setBorderPainted(false);
		exportButton.setEnabled(false);
		exportButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exportPressed(evt);
			}
		});

		jScrollPane1.setBackground(java.awt.Color.black);
		jScrollPane1.setBorder(javax.swing.BorderFactory.createLineBorder(
				java.awt.Color.darkGray, 4));

		effectsListPanel.setBackground(java.awt.Color.black);
		effectsListPanel.setLayout(new javax.swing.BoxLayout(effectsListPanel,
				javax.swing.BoxLayout.PAGE_AXIS));
		jScrollPane1.setViewportView(effectsListPanel);

		removeEffects.setForeground(new java.awt.Color(255, 255, 255));
		removeEffects.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/mvc/gui/resources/clear.png"))); //$NON-NLS-1$
		removeEffects.setDisabledIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/mvc/gui/resources/clear disabled.png"))); //$NON-NLS-1$
		removeEffects.setText(messages.getString("removeAllEffects")); //$NON-NLS-1$
		removeEffects.setFont(new Font("Curier", 0, 14)); //$NON-NLS-1$
		removeEffects.setHorizontalTextPosition(SwingConstants.CENTER);
		removeEffects.setBorderPainted(false);
		removeEffects.setEnabled(false);
		removeEffects.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				removeAllEffects(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jScrollPane1,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				256,
																				Short.MAX_VALUE))
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																layout
																		.createSequentialGroup()
																		.addContainerGap(
																				109,
																				Short.MAX_VALUE)
																		.addComponent(
																				removeEffects))
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addGroup(
																				layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								exportButton,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								256,
																								Short.MAX_VALUE)
																						.addComponent(
																								effectPanel,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								256,
																								Short.MAX_VALUE)
																						.addComponent(
																								importButton,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								256,
																								Short.MAX_VALUE))))
										.addContainerGap()));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addComponent(
												importButton,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												65,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												effectPanel,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												204,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												exportButton,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												60,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												131, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(removeEffects)
										.addContainerGap()));
	}

	protected void removeAllEffects(ActionEvent evt) {
		presenter.removeAllEffects();

	}

	/**
	 * calls on presenter when importButton is pressed
	 * 
	 * @param evt
	 */
	private void importPressed(java.awt.event.ActionEvent evt) {
		presenter.importPressed();
	}

	/**
	 * calls on presenter when exportButton is pressed
	 * 
	 * @param evt
	 */
	private void exportPressed(java.awt.event.ActionEvent evt) {
		presenter.exportPressed();
	}

	/**
	 * get method for effectPanel
	 * 
	 * @return effectPanel
	 */
	JPanel getEffectPanel() {
		return effectPanel;
	}

	/**
	 * get method for effectsListPanel
	 * 
	 * @return effectsListPanel
	 */
	JPanel getEffectsListPanel() {
		return effectsListPanel;
	}

	JButton getRemoveAllEffectsButton() {
		return removeEffects;
	}

	JButton getExportButton() {
		return exportButton;
	}

}
