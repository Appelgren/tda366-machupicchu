package mvc.gui.impl.mainframe;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import mvc.core.IMachuPicchu;
import mvc.core.impl.MachuPicchu;
import mvc.gui.utils.ImagePanel;

/**
 * 
 * @author Berget
 */
@SuppressWarnings("serial")
public final class MainFrame extends javax.swing.JFrame {

	private ImagePanel mainStage;
	private mvc.gui.impl.thumbnailspanel.ThumbnailsPanel thumbnailsPanel;
	private mvc.gui.impl.toolpanel.ToolPanel toolPanel;
	private transient IMachuPicchu machu;

	/**
	 * Creates MainFramePresenter.
	 */
	public MainFrame() {
		machu = MachuPicchu.getInstance();

		this.setTitle("Machu Picchu"); //$NON-NLS-1$
		BufferedImage image = null;
		try {
			image = ImageIO.read(getClass().getResource(
					"/mvc/gui/resources/machuPicchuicon.png")); //$NON-NLS-1$
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setIconImage(image);

		initComponents();
		new MainFramePresenter(machu, this);

	}

	private void initComponents() {

		toolPanel = new mvc.gui.impl.toolpanel.ToolPanel(machu);
		thumbnailsPanel = new mvc.gui.impl.thumbnailspanel.ThumbnailsPanel(
				machu);
		mainStage = new ImagePanel();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		mainStage.setBackground(Color.BLACK);

		this.getContentPane().setBackground(Color.BLACK);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(thumbnailsPanel,
								javax.swing.GroupLayout.DEFAULT_SIZE, 821,
								Short.MAX_VALUE)
						.addGroup(
								layout
										.createSequentialGroup()
										.addComponent(
												toolPanel,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												mainStage,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addContainerGap()));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								layout
										.createSequentialGroup()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																toolPanel,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																540,
																Short.MAX_VALUE)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addGap(
																				20,
																				20,
																				20)
																		.addComponent(
																				mainStage,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												thumbnailsPanel,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)));

		pack();
	}

	/**
	 * get method for mainStage
	 * 
	 * @return mainStage
	 */
	ImagePanel getMainStage() {
		return mainStage;
	}

}
