package mvc.effects;

import java.awt.image.BufferedImage;

public interface IEffect {
	/**
	 * Applies the effect to the BufferedImage object and returns a new
	 * BufferedImage object.
	 * 
	 * @param img
	 *            the source image
	 * @return the destination image
	 */
	public BufferedImage applyEffect(BufferedImage img);
}
