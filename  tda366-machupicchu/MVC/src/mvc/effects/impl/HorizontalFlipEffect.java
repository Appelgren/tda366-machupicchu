package mvc.effects.impl;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.util.ResourceBundle;

import mvc.effects.INonParameterizedEffect;
import mvc.utils.MPResourceBundle;

public class HorizontalFlipEffect implements INonParameterizedEffect {

	private ResourceBundle messages;

	@Override
	public BufferedImage applyEffect(BufferedImage img) {

		AffineTransform aft = AffineTransform.getScaleInstance(-1, 1);
		aft.translate(-img.getWidth(), 0);
		BufferedImageOp horizontalFlip = new AffineTransformOp(aft, null);

		return horizontalFlip.filter(img, null);
	}

	@Override
	public String toString() {
		messages = MPResourceBundle.getResourceBundle();
		return messages.getString("horizontalflip"); //$NON-NLS-1$
	}

}
