package mvc.effects.impl;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.util.ResourceBundle;

import mvc.effects.INonParameterizedEffect;
import mvc.utils.MPResourceBundle;

public class BlurEffect implements INonParameterizedEffect {

	private ResourceBundle messages;

	@Override
	public BufferedImage applyEffect(BufferedImage img) {

		float[] blurKernel = { 0f, 0.2f, 0f, 0.2f, 0.2f, 0.2f, 0f, 0.2f, 0f };

		BufferedImageOp blur = new ConvolveOp(new Kernel(3, 3, blurKernel));

		return blur.filter(img, null);
	}

	@Override
	public String toString() {
		messages = MPResourceBundle.getResourceBundle();
		return messages.getString("blur"); //$NON-NLS-1$
	}

}
