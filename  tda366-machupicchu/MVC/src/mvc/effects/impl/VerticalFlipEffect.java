package mvc.effects.impl;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.util.ResourceBundle;

import mvc.effects.INonParameterizedEffect;
import mvc.utils.MPResourceBundle;

public class VerticalFlipEffect implements INonParameterizedEffect {

	private ResourceBundle messages;

	@Override
	public BufferedImage applyEffect(BufferedImage img) {
		AffineTransform aft = AffineTransform.getScaleInstance(1, -1);
		aft.translate(0, -img.getHeight());
		BufferedImageOp verticalFlip = new AffineTransformOp(aft, null);

		return verticalFlip.filter(img, null);
	}

	@Override
	public String toString() {
		messages = MPResourceBundle.getResourceBundle();
		return messages.getString("verticalflip"); //$NON-NLS-1$
	}
}
