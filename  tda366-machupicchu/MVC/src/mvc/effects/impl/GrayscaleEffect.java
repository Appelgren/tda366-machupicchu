package mvc.effects.impl;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ResourceBundle;

import mvc.effects.INonParameterizedEffect;
import mvc.utils.MPResourceBundle;

public class GrayscaleEffect implements INonParameterizedEffect {

	private ResourceBundle messages;

	@Override
	public BufferedImage applyEffect(BufferedImage img) {
		int greyColorType = BufferedImage.TYPE_BYTE_GRAY;
		int originalColorType = img.getType();

		return filter(filter(img, greyColorType), originalColorType);
	}

	private BufferedImage filter(BufferedImage img, int colorType) {
		BufferedImage img2 = new BufferedImage(img.getWidth(), img.getHeight(),
				colorType);
		Graphics g2 = img2.getGraphics();
		g2.drawImage(img, 0, 0, null);
		g2.dispose();
		return img2;
	}

	@Override
	public String toString() {
		messages = MPResourceBundle.getResourceBundle();
		return messages.getString("greyscale"); //$NON-NLS-1$
	}

}
