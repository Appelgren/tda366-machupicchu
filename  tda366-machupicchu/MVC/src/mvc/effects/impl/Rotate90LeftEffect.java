package mvc.effects.impl;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.util.ResourceBundle;

import mvc.effects.INonParameterizedEffect;
import mvc.utils.MPResourceBundle;

public class Rotate90LeftEffect implements INonParameterizedEffect {

	private ResourceBundle messages;

	@Override
	public BufferedImage applyEffect(BufferedImage img) {

		AffineTransform aft = AffineTransform.getQuadrantRotateInstance(3);
		aft.translate(-img.getWidth(), 0);
		BufferedImageOp rotate90right = new AffineTransformOp(aft, null);

		return rotate90right.filter(img, null);
	}

	@Override
	public String toString() {
		messages = MPResourceBundle.getResourceBundle();
		return messages.getString("rotate90left"); //$NON-NLS-1$
	}

}
