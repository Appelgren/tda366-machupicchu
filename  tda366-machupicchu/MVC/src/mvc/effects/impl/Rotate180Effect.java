package mvc.effects.impl;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.util.ResourceBundle;

import mvc.effects.INonParameterizedEffect;
import mvc.utils.MPResourceBundle;

public class Rotate180Effect implements INonParameterizedEffect {

	private ResourceBundle messages;

	@Override
	public BufferedImage applyEffect(BufferedImage img) {

		AffineTransform aft = AffineTransform.getQuadrantRotateInstance(2);
		aft.translate(-img.getWidth(), -img.getHeight());
		/*
		 * AffineTransform aft = AffineTransform.getQuadrantRotateInstance(2,
		 * (double) img.getWidth() / 2, (double) img.getHeight() / 2);
		 */
		BufferedImageOp rotate180 = new AffineTransformOp(aft, null);

		return rotate180.filter(img, null);
	}

	@Override
	public String toString() {
		messages = MPResourceBundle.getResourceBundle();
		return messages.getString("rotate180"); //$NON-NLS-1$
	}

}
