package mvc.effects;

/**
 * Interface used by non parameterized effects.
 * 
 * @author joellundell
 * 
 */
public interface INonParameterizedEffect extends IEffect {

}
