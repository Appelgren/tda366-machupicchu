package mvc.core;

import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import mvc.effects.IEffect;
import mvc.image.MachuImage;
import mvc.utils.FileErrorException;

/**
 * The interface the application's model is expected to use.
 * 
 * @author Joel
 * 
 */

public interface IMachuPicchu {

	/**
	 * Import image files into the application.
	 */
	public void importImages(int width, int height);

	/**
	 * Export selected images from the application.
	 */
	public void exportImages();

	/**
	 * Removes images from library.
	 * 
	 * @param list
	 *            List of images to be removed
	 */
	public void removeImage(List<MachuImage> list);

	/**
	 * Returns a list of all images in the applications library.
	 * 
	 * @return List with MachuImages
	 */
	public List<MachuImage> getImageList();

	/**
	 * Adds a property change listener to receive events when core is modified.
	 * 
	 * @param l
	 *            The object that listens on the core
	 */
	public void addPropertyChangeListener(PropertyChangeListener l);

	/**
	 * Removes the listener from the list of objects receiving events when core
	 * is modified.
	 * 
	 * @param l
	 *            The object to be removed from list of listeners.
	 */
	public void removePropertyChangeListener(PropertyChangeListener l);

	/**
	 * Returns the list of images currently set as selected in the library.
	 * 
	 * @return List with MachuImages
	 */
	public List<MachuImage> getSelectedImages();

	/**
	 * Set the list of images that is selected.
	 * 
	 * @param list
	 *            with selected images
	 */
	public void setSelectedImages(List<MachuImage> list);

	/**
	 * adds a MachuImage to the selectedImages.
	 * 
	 * @param i
	 *            MachuImage to be added
	 */
	public void addToSelectedImages(MachuImage i);

	/**
	 * Applies effect to all selected images.
	 * 
	 * @param effect
	 *            to be applied
	 */

	public void applyEffect(IEffect effect);

	/**
	 * Returns a list with all the effects implemented in the application.
	 * 
	 * @return List with IEffects
	 */
	public List<IEffect> getListWithEffects();

	/**
	 * Method called to make a controlled shutdown. Called automatically from
	 * the shutdown-hook added to the JVM in main.
	 */
	public void shutdown();

	/**
	 * Returns a scaled instance of the image with the proportions specified.
	 * 
	 * @param img
	 *            Image to be scaled
	 * @param width
	 *            The scaled image's width
	 * @param height
	 *            The scaled image's height
	 * @return The scaled image
	 * @throws IOException
	 */
	public Image getScaledImage(Image img, int width, int height);

	/**
	 * Returns a full size image from the MachuImage.
	 * 
	 * @param img
	 *            MachuImage to get the fullsize image of
	 * @return A full size image
	 * @throws FileErrorException
	 * @throws IOException
	 */
	public Image getFullSizeImage(MachuImage img);

	/**
	 * Process the MachuImage with the height and width specified.
	 * 
	 * @param mi
	 *            Image to be processed
	 * @param width
	 *            Width of the image returned
	 * @param height
	 *            Height of the image returned
	 * @return The processed image
	 */
	public Image processImage(MachuImage mi, int width, int height);

	/**
	 * Set the language of the application.
	 * 
	 * @param currentLocale
	 *            Locale used to set the language
	 */
	public void setLanguageOfApplication(Locale currentLocale);

	/**
	 * Get the current locale in use.
	 * 
	 * @return Current locale
	 */
	public Locale getLanguageOfApplication();

	/**
	 * Tests so that no MachuImages were deleted or damaged
	 * 
	 * @param list
	 *            with MachuImages to test
	 * @return
	 */
	public List<MachuImage> testImages(List<MachuImage> list);

	/**
	 * TODO: Correct doc Updates the selected Main-Stage image according to its
	 * EffectsListIndex.
	 * 
	 * @param index
	 */
	public void updateMainStageImage(int thumbnailWidth, int thumbnailHeight);

}
