package mvc.core.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mvc.effects.IEffect;
import mvc.effects.impl.*;
import mvc.image.MachuImage;

/**
 * Part of the core, handling logic when using effects.
 * 
 * @author joellundell
 * 
 */
public class EffectsHandler {

	private List<IEffect> listWithEffects;

	/**
	 * Creates an effects handler.
	 */
	public EffectsHandler() {
		listWithEffects = new LinkedList<IEffect>();
		setListWithEffects();
	}

	/**
	 * Adds the effect to the effects list of each image in the image list.
	 * 
	 * @param img
	 *            List of images
	 * @param effect
	 *            Effect to be added
	 */
	public void addEffect(List<MachuImage> selectedImages, IEffect effect) {
		for (MachuImage mi : selectedImages) {
			if (mi.getIndexOfCurrentEffect() != mi.getEffectsList().size() - 1) {
				int startIndex = 0;
				int endIndex = mi.getIndexOfCurrentEffect() + 1;

				List<IEffect> list = new ArrayList<IEffect>(mi.getEffectsList()
						.subList(startIndex, endIndex));
				mi.setEffectsList(list);
			}
			mi.addEffect(effect);
			mi.setIndexOfCurrentEffect(mi.getIndexOfCurrentEffect() + 1);
		}
	}

	public void removeLastEffect(MachuImage img) {
		img.getEffectsList().remove(img.getEffectsList().size() - 1);
	}

	private void setListWithEffects() {
		listWithEffects.add(new GrayscaleEffect());
		listWithEffects.add(new InvertEffect());
		listWithEffects.add(new BrightenEffect());
		listWithEffects.add(new Rotate180Effect());
		listWithEffects.add(new Rotate90LeftEffect());
		listWithEffects.add(new Rotate90RightEffect());
		listWithEffects.add(new HorizontalFlipEffect());
		listWithEffects.add(new VerticalFlipEffect());
	}

	public List<IEffect> getListWithEffects() {
		return new LinkedList<IEffect>(listWithEffects);
	}
}
