package mvc.core.impl;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
//import java.util.ResourceBundle;

import mvc.exporter.IExporter;
import mvc.image.MachuImage;
import mvc.imageProcessor.IImageProcessor;
import mvc.utils.FileErrorException;
//import mvc.utils.MPResourceBundle;

import com.google.inject.Inject;

/**
 * Part of the core that handles the logic when exporting images.
 * 
 * @author Joel
 * 
 */
public class ExportHandler {
	private final IExporter exporter;
	private final IImageProcessor imageProcessor;

	/**
	 * Constructor used by Google Guice.
	 * 
	 * @param exporter
	 */
	@Inject
	public ExportHandler(IExporter exporter, IImageProcessor imageProcessor) {
		this.exporter = exporter;
		this.imageProcessor = imageProcessor;

	}

	/**
	 * Exports the following images and allows the user to decide where.
	 * 
	 * @param imageList
	 *            Images to be exported
	 * @throws IOException
	 *             Throws an IOException if the writing to disk goes wrong
	 * @throws FileErrorException
	 *             Throws an FileErrorException if file can't be read.
	 */
	public void exportImages(List<MachuImage> imageList) throws IOException,
			FileErrorException {
		//ResourceBundle messages = MPResourceBundle.getResourceBundle();
		if (exporter.prepareSave()) {
			for (MachuImage mi : imageList) {
				//long time = System.currentTimeMillis();
				//	System.out.println(messages.getString("exporting") //$NON-NLS-1$
				//		+ mi.getFile().getName()); //$NON-NLS-1$
				BufferedImage coolImage;
				try {
					coolImage = imageProcessor.processImage(mi, -1, -1);

					exporter.saveImage(coolImage, mi.getFile().getName());
					//					System.out.println("		" //$NON-NLS-1$
					// + (System.currentTimeMillis() - time)
					//							+ messages.getString("milliseconds")); //$NON-NLS-1$
				} catch (FileErrorException e) {
					throw new FileErrorException(mi.getFile().getName());
				}
			}
		}
	}
}
