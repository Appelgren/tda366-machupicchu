package mvc.core.impl;

import mvc.core.IMachuPicchu;

/**
 * A shutdown interceptor can be used to perform a controlled shutdown
 * 
 * @author joellundell
 * 
 */
public class ShutdownInterceptor extends Thread {

	IMachuPicchu machu;

	/**
	 * Creates a ShutdownInterceptor that can be inserted into the JVM-instance.
	 * 
	 * @param machu
	 *            An instance of the application to be noted when application is
	 *            shutting down.
	 */
	public ShutdownInterceptor(IMachuPicchu machu) {
		this.machu = machu;
	}

	/**
	 * Called when the JVM-instance is shutting down.
	 */
	@Override
	public void run() {

		machu.shutdown();

	}

}
