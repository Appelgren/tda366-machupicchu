package mvc.core.impl;

import java.util.LinkedList;
import java.util.List;

import mvc.image.MachuImage;
import mvc.library.ILibrary;

import com.google.inject.Inject;

/**
 * Part of the core, handling the logic to use the library.
 *
 * @author Joel
 *
 */
public class LibraryHandler {

	private ILibrary<MachuImage> library;

	/**
	 * Constructor used by Google Guice.
	 *
	 * @param library
	 */
	@Inject
	public LibraryHandler(ILibrary<MachuImage> library) {

		this.library = library;

	}

	/**
	 * Adds a list of images to the library
	 *
	 * @param list
	 *            List with MachuImage's to be added.
	 */
	public synchronized void addImages(List<MachuImage> list) {
		for (MachuImage i : list) {
			addImage(i);
		}

	}

	/**
	 * Adds a single image to the library.
	 *
	 * @param img
	 *            Image to be added
	 */
	public synchronized void addImage(MachuImage img) {
		library.add(img);
	}

	/**
	 * Removes the images in the list from the library.
	 *
	 * @param list
	 *            List with MachuImages to be removed from the library.
	 */
	public synchronized void removeImages(List<MachuImage> list) {
		for (MachuImage i : list) {
			library.remove(i);
		}

		// If there are images in the library sets the first one as selected
		if (library.getImageList().size() > 0) {
			List<MachuImage> temp = new LinkedList<MachuImage>();
			temp.add(library.getImageList().get(0));
			library.setSelectedImages(temp);
		}
	}

	public synchronized void removeImage(MachuImage i) {
		library.remove(i);
	}

	/**
	 * Returns a list with the images in the library.
	 *
	 * @return List with MachuImages
	 */
	public synchronized List<MachuImage> getImages() {
		return library.getImageList();
	}

	/**
	 * Sets the list with images that currently selected in the application.
	 *
	 * @param list
	 *            List of MachuImages to be set as selected
	 */
	public synchronized void setSelectedImages(List<MachuImage> list) {
		library.setSelectedImages(list);
	}

	/**
	 * Returns a list with the images currently set as selected.
	 *
	 * @return List of MachuImages
	 */
	public synchronized List<MachuImage> getSelectedImages() {

		return library.getSelectedImages();
	}

	public synchronized void addToSelectedImages(MachuImage i) {
		library.addToSelectedImages(i);
	}

}
