package mvc.core.impl;

import mvc.exporter.IExporter;
import mvc.exporter.impl.LocalFileExporter;
import mvc.image.MachuImage;
import mvc.imageProcessor.IImageProcessor;
import mvc.imageProcessor.impl.ImageProcessor;
import mvc.importer.IImporter;
import mvc.importer.impl.LocalFileImporter;
import mvc.library.ILibrary;
import mvc.library.impl.Library;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;

/**
 * The module where the guice bindings are made.
 * 
 * @author Joel
 * 
 */

public class MPModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(IImporter.class).to(LocalFileImporter.class);
		bind(IImageProcessor.class).to(ImageProcessor.class);
		bind(IExporter.class).to(LocalFileExporter.class);
		bind(new TypeLiteral<ILibrary<MachuImage>>() {
		}).toInstance(new Library<MachuImage>());
	}
}
