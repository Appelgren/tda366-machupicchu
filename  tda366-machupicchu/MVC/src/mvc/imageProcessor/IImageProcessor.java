package mvc.imageProcessor;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import mvc.image.MachuImage;
import mvc.utils.FileErrorException;

/**
 * Interface the imageprocessor is expected to use.
 * 
 * @author Joel
 * 
 */
public interface IImageProcessor {

	/**
	 * Processes all the effects in the MachuImage effects list and returns a
	 * BufferdImage of given width and height.
	 * 
	 * @param img
	 *            MachuImage to be processed
	 * @param width
	 *            Width of the image returned
	 * @param height
	 *            Height of the image returned
	 * @return A processed buffered image
	 * @throws IOException
	 *             If file reading goes wrong when acquiring original file
	 * @throws FileErrorException
	 *             If file is damaged or not an actual image file
	 */
	public BufferedImage processImage(MachuImage img, int width, int height)
			throws IOException, FileErrorException;

	/**
	 * Processes all the thumbnail images in a list of MachuImages by applying
	 * the last added effect to each image.
	 * 
	 * @param selectedImages
	 *            The list of MachuImages to be processed
	 */
	public void processThumbnailImages(List<MachuImage> selectedImages);

	/**
	 * Scales an Image to preferred size. If width is of negative value scales
	 * the Image with a 1:1 ratio according to the height. And vice versa.
	 * 
	 * @param img
	 *            Image to be processed
	 * @param width
	 *            The width of the scaled image
	 * @param height
	 *            The height of the scaled image
	 * @return An scaled image.
	 * @throws IOException
	 *             If file reading goes wrong when acquiring original file
	 */
	public Image createScaledImage(Image img, int width, int height)
			throws IOException;

	/**
	 * Returns the full size image stored in the applications temporary folder.
	 * 
	 * @param img
	 *            Image to be retrieved
	 * @return The full size image
	 * @throws IOException
	 *             If file reading goes wrong when acquiring original file
	 * @throws FileErrorException
	 *             If file is damaged or not an actual image file
	 */
	public Image getFullSizeImage(MachuImage img) throws IOException,
			FileErrorException;

	/**
	 * 
	 * @param image
	 *            image of witch the thumbnail is to be created
	 * @param width
	 *            width of returning image
	 * @param height
	 *            height of returning image
	 * @return
	 * @throws IOException
	 * @throws FileErrorException
	 */
	Image createThumbnail(MachuImage image, int width, int height)
			throws IOException, FileErrorException;

}
