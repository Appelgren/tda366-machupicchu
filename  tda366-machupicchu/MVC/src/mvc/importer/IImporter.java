package mvc.importer;

import java.io.File;
import java.util.List;

/**
 * The interface all import implementations use.
 * 
 * @author Joel
 * 
 */
public interface IImporter {

	/**
	 * Returns a list with files imported.
	 * 
	 * @return List of Files.
	 */
	public List<File> getFiles();
}
