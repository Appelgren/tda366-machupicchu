package mvc.importer.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mvc.importer.IImporter;
import mvc.iogui.IFileChooser;
import mvc.iogui.impl.LocalFileChooser;

/**
 * A local file importer. Import files from local drives.
 * 
 * @author Joel
 * 
 */
public class LocalFileImporter implements IImporter {
	private IFileChooser fc;

	/**
	 * Creates a LocalFileImporter.
	 */
	public LocalFileImporter() {
		this(new LocalFileChooser(LocalFileChooser.DialogType.OPEN_DIALOG));
	}

	public LocalFileImporter(IFileChooser fc) {
		this.fc = fc;
	}

	/**
	 * Returns a list of Files imported from local disc.
	 */
	public List<File> getFiles() {
		List<String> afe = new LinkedList<String>();
		for (String s : javax.imageio.ImageIO.getReaderFileSuffixes()) {
			afe.add(s);
		}
		if (fc.showFileChooser(afe)) {
			return fc.getFiles();
		} else {
			return new ArrayList<File>();
		}
	}

}
