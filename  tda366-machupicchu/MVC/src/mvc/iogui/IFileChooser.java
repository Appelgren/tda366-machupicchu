package mvc.iogui;

import java.io.File;
import java.util.List;

public interface IFileChooser {
	/**
	 * method to get one file (in save dialogs), must be used after
	 * showFileChooser
	 * 
	 * @return
	 */
	public File getFile();

	/**
	 * method to get the chosen files, must be used after showFileChooser
	 * 
	 * @return
	 */
	public List<File> getFiles();

	/**
	 * gets the selected file extension
	 * 
	 * @return
	 */
	public String getFileExtension();

	/**
	 * shows a filechooser
	 * 
	 * @param afe
	 *            list of accepted file extensions
	 * @return
	 */
	boolean showFileChooser(List<String> afe);

}
