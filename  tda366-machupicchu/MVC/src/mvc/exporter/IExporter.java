package mvc.exporter;

import java.awt.Image;
import java.io.IOException;

/**
 * The interface for the exporter function of the application.
 * 
 * @author Joel
 * 
 */
public interface IExporter {
	/**
	 * Prepare to save the image.
	 * 
	 * @return Boolean true if prepareSave was successful, otherwise false
	 */
	public boolean prepareSave();

	/**
	 * Saves the image.
	 * 
	 * @param image
	 *            Image to be saved
	 * @param filename
	 *            Filename
	 * @throws IOException
	 *             Throws an IOException if the writing to disk goes wrong
	 */
	public void saveImage(Image image, String filename) throws IOException;
}
