package mvc.exporter.impl;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List; //import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import mvc.exporter.IExporter;
import mvc.iogui.IFileChooser;
import mvc.iogui.impl.LocalFileChooser;
import mvc.iogui.impl.LocalFileChooser.DialogType;

public class LocalFileExporter implements IExporter {
	private IFileChooser fileChooser;
	private String filePath;
	private String fileExt;

	public LocalFileExporter() {
		this(new LocalFileChooser(DialogType.SAVE_DIALOG));
	}

	public LocalFileExporter(IFileChooser fileChooser) {
		this.fileChooser = fileChooser;
	}

	@Override
	public boolean prepareSave() {
		List<String> afe = new LinkedList<String>();
		for (String s : javax.imageio.ImageIO.getWriterFileSuffixes()) {
			afe.add(s);
		}
		if (fileChooser.showFileChooser(afe)) {
			filePath = fileChooser.getFile().getAbsolutePath();
			fileExt = fileChooser.getFileExtension();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void saveImage(Image image, String filename) throws IOException {

		if (image instanceof RenderedImage) {
			ImageIO.write((RenderedImage) image, fileExt, new File(filePath
					+ File.separator
					+ filename.substring(0, filename.lastIndexOf(".")) + "." //$NON-NLS-1$ //$NON-NLS-2$
					+ fileExt));
		}
	}

}
