package mvc.library.impl;

import java.util.LinkedList;
import java.util.List;

import mvc.library.ILibrary;

/**
 * The library use by the application.
 * 
 * @author Joel
 * 
 */
public class Library<E> implements ILibrary<E> {

	private List<E> imageList;
	private List<E> selectedImages;

	public Library() {
		imageList = new LinkedList<E>();
		selectedImages = new LinkedList<E>();

	}

	@Override
	public void add(E image) {

		imageList.add(image);

	}

	@Override
	public List<E> getImageList() {

		return new LinkedList<E>(imageList);
	}

	@Override
	public void remove(E image) {
		imageList.remove(image);
		selectedImages.remove(image);
	}

	@Override
	public void setSelectedImages(List<E> list) {
		selectedImages = list;

	}

	@Override
	public List<E> getSelectedImages() {
		return new LinkedList<E>(selectedImages);
	}

	@Override
	public void addToSelectedImages(E i) {
		selectedImages.add(i);

	}

}
