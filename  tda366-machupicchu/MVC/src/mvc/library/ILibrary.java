package mvc.library;

import java.util.List;

/**
 * Interface that specifies what a library implementation must be able to
 * perform.
 * 
 * @author joellundell
 * 
 */
public interface ILibrary<E> {

	/**
	 * Returns the list with images in the library
	 * 
	 * @return List<MachuImage>
	 */
	public List<E> getImageList();

	/**
	 * Remove image from library
	 * 
	 * @param image
	 *            Image to be removed
	 */
	public void remove(E image);

	/**
	 * Add image to library
	 * 
	 * @param image
	 *            Image do be added
	 */
	public void add(E image);

	/**
	 * Set the list with images that is selected in the model.
	 * 
	 * @param list
	 *            List<MachuImage> with images selected by the user
	 */
	public void setSelectedImages(List<E> list);

	/**
	 * Returns the images set as selected in the model.
	 * 
	 * @return List<MachuImage>
	 */
	public List<E> getSelectedImages();

	/**
	 * Adds a single MachuImage to SelectedImages
	 * 
	 * @param i
	 */
	public void addToSelectedImages(E i);
}
