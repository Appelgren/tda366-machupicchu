package mvc.utils;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Contains the resourcebundle used to create an internationalized application
 * 
 * @author joellundell
 * 
 */

public final class MPResourceBundle {

	private static ResourceBundle messages;

	private MPResourceBundle() {

	}

	/**
	 * Set the ResourceBundle. Can be changed at run-time.
	 * 
	 * @param currentLocale
	 *            The locale you want to set
	 */

	public static void setResourceBundle(Locale currentLocale) {

		messages = ResourceBundle.getBundle("MessagesBundle", currentLocale); //$NON-NLS-1$
		Locale.setDefault(currentLocale);

	}

	/**
	 * Returns the Resource Bundle currently in use. Use setResourceBundle to
	 * change locale.
	 * 
	 * @return Current Resource Bundle
	 */
	public static ResourceBundle getResourceBundle() {
		return messages;

	}
}
