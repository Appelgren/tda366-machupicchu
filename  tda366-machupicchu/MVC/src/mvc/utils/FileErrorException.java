package mvc.utils;

public class FileErrorException extends Exception {
	private static final long serialVersionUID = 1L;
	private String message;

	public FileErrorException() {
	}

	public FileErrorException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
