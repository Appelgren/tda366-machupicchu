package mvc;

import java.util.Locale;

import javax.swing.SwingUtilities;

import mvc.core.impl.MachuPicchu;
import mvc.core.impl.ShutdownInterceptor;
import mvc.gui.impl.mainframe.MainFrame;
import mvc.utils.MPResourceBundle;

/**
 * The application entry point
 *
 *
 * @author joellundell
 *
 */
public class Main {

	/**
	 * Argument 0 and 1 is location arguments (i.e en EN or fr FR or se SV)
	 * @param args
	 */
	public static void main(final String[] args) {

		setLocale(args);

		// Creates a shutdownInterceptor and adds it to the JVM instance
		ShutdownInterceptor shutdownInterceptor = new ShutdownInterceptor(
				MachuPicchu.getInstance());
		Runtime.getRuntime().addShutdownHook(shutdownInterceptor);

		// This is the way to construct the GUI
		// (in the event thread)
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainFrame mf = new MainFrame();
				mf.setLocationRelativeTo(null);
				mf.setVisible(true);
			}
		});

	}

	// Sets the ResourceBundle on start-up
	private static void setLocale(String[] args) {
		Locale currentLocale;

		// If no locale arguments on start-up sets locale to JVM-default
		if (args.length == 0 || args[0] == null || args[1] == null) {
			currentLocale = Locale.getDefault();
		} else {
			currentLocale = new Locale(args[0], args[1]);
		}

		MPResourceBundle.setResourceBundle(currentLocale);

	}

}
