package mvc.image;

import java.awt.Image;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import mvc.effects.IEffect;

public class MachuImage {
	private File file;
	private Image thumbnail;
	private List<IEffect> effectsList;
	private int iOCE;

	public MachuImage(File file, Image thumbnail) {
		this.file = file;
		this.thumbnail = thumbnail;
		effectsList = new LinkedList<IEffect>();
		iOCE = -1;
	}

	public MachuImage(File newFile) {
		this(newFile, null);
	}

	public File getFile() {
		return file;
	}

	public Image getThumbnail() {
		return thumbnail;
	}

	public List<IEffect> getEffectsList() {
		return new LinkedList<IEffect>(effectsList);
	}

	public void addEffect(IEffect effect) {
		effectsList.add(effect);
	}

	public int getIndexOfCurrentEffect() {
		return iOCE;
	}

	public void setIndexOfCurrentEffect(int i) {
		iOCE = i;
	}

	public void setThumbnail(Image thumbnail) {
		this.thumbnail = thumbnail;
	}

	public void setEffectsList(List<IEffect> list) {
		effectsList = list;
	}
}