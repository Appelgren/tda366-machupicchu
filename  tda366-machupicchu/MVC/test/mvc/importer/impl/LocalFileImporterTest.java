package mvc.importer.impl;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import mvc.StringHolder;
import mvc.importer.IImporter;
import mvc.iogui.IFileChooser;
import mvc.iogui.MockFileChooser;

import org.junit.Before;
import org.junit.Test;

public class LocalFileImporterTest {
	private IImporter lfi;
	private IFileChooser fileChooser;
	private List<File> fileList;

	@Before
	public void setUp() throws Exception {
		File f1 = new File(StringHolder.blommabakgrund);
		File f2 = new File(StringHolder.after);
		File f3 = new File(StringHolder.before);
		fileList = new LinkedList<File>();
		fileList.add(f1);
		fileList.add(f2);
		fileList.add(f3);
		fileChooser = new MockFileChooser(null, fileList, null);

		lfi = new LocalFileImporter(fileChooser);
	}

	@Test
	public void testGetFiles() {
		List<File> fileList2 = lfi.getFiles();

		boolean bool = true;
		for (File f : fileList2) {
			if (!fileList.contains(f)) {
				bool = false;
			}
		}
		assertTrue(bool);
	}

}
