package mvc.utils;

import static org.junit.Assert.assertTrue;

import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestAllMethodsInResourceBundle {

	private ResourceBundle resourceBundle;
	private Locale currentLocale;

	@Before
	public void init() {
		currentLocale = new Locale("fr", "FR"); //$NON-NLS-1$ //$NON-NLS-2$
		MPResourceBundle.setResourceBundle(currentLocale);
		resourceBundle = MPResourceBundle.getResourceBundle();
	}

	@After
	public void uninit() {
		resourceBundle = null;
		currentLocale = null;

	}

	@Test
	public void testResourceBundle() {
		assertTrue(resourceBundle.getLocale().equals(currentLocale));
	}

}
