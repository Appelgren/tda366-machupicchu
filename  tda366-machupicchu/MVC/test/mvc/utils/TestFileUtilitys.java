package mvc.utils;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import mvc.StringHolder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestFileUtilitys {

	private File source;
	private File source2;
	private File destination;
	private File destination2;
	private File tempDir;

	private String PATH_NAME;
	private String PATH_NAME2;
	private String destPathName;
	private String destPathName2;
	private String tempDirName;

	@Before
	public void init() {

		PATH_NAME = StringHolder.blommabakgrund;
		PATH_NAME2 = StringHolder.after;
		destPathName = StringHolder.COPIED_IMAGE1_PATH_IN_UTILS;
		destPathName2 = StringHolder.COPIED_IMAGE2_PATH_IN_UTILS;
		tempDirName = StringHolder.TEMPDIR_IN_UTILS;

		source = new File(PATH_NAME);
		source2 = new File(PATH_NAME2);
		destination = new File(destPathName);
		destination2 = new File(destPathName2);
		tempDir = new File(tempDirName);

	}

	@After
	public void uninit() {

		PATH_NAME = null;
		PATH_NAME2 = null;
		destPathName = null;
		source = null;
		destination = null;
		tempDir = null;
	}

	@SuppressWarnings("nls")
	@Test
	public void testLibrary() throws IOException {

		// Creates a temporary directory in "test/mvc/utils/"
		if (!tempDir.isDirectory()) {
			if (!tempDir.mkdir()) {
				fail("Directory could not be created");
			}

		}

		// Copies the files to the directory
		FileUtilitys.copyFile(source, destination);
		FileUtilitys.copyFile(source2, destination2);

		// Checks if the destination file is a file and the directory is a
		// directory
		// and then deletes them both
		if (destination.isFile() && tempDir.isDirectory()) {
			FileUtilitys.removeFileDirectory(tempDirName);
		}
		// Checks that destination file no longer is a file and that tempDir no
		// longer is a directory
		assertTrue(!tempDir.isDirectory() && !destination.isFile());

	}
}
