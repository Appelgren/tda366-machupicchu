package mvc.imageProcessor.impl;

import static org.junit.Assert.assertTrue;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import mvc.StringHolder;
import mvc.image.MachuImage;
import mvc.imageProcessor.IImageProcessor;
import mvc.imageProcessor.impl.ImageProcessor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestingImageProcessor {

	private MachuImage machuImage;
	private Image image;
	private String PATH_NAME;
	private File path;
	private IImageProcessor imageProcessor;

	@Before
	public void init() throws IOException {

		imageProcessor = new ImageProcessor();
		PATH_NAME = StringHolder.blommabakgrund;
		path = new File(PATH_NAME);
		image = javax.imageio.ImageIO.read(path);
		machuImage = new MachuImage(path, image);

	}

	@After
	public void uninit() {

		imageProcessor = null;
		PATH_NAME = null;
		path = null;
		image = null;
		machuImage = null;

	}

	@Test
	public void testImageProcessor() throws Exception {

		// Sets a thumbnail to the machuImage
		Image tempImage = imageProcessor.createScaledImage(image, 400, 500);
		machuImage.setThumbnail(tempImage);

		// Checks so the thumbnail has the same size as the thumbnail
		// previously set
		if (machuImage.getThumbnail().getWidth(null) == 400
				&& machuImage.getThumbnail().getHeight(null) == 500
				&& machuImage.getThumbnail().getSource().equals(
						tempImage.getSource())) {

			// Checks if the full size image returned by the getFullSizeImage is
			// the same image when created the MachuImage and that it is of the
			// same size
			Image fullSizeImage = imageProcessor.getFullSizeImage(machuImage);
			if (!fullSizeImage.equals(image)
					&& fullSizeImage.getWidth(null) == image.getWidth(null)
					&& fullSizeImage.getHeight(null) == image.getHeight(null)) {

				// Processes two images and checks that there are two different
				// images returned
				Image processedImage = imageProcessor.processImage(machuImage,
						-1, -1);
				Image processedImage2 = imageProcessor.processImage(machuImage,
						-1, -1);
				if (!processedImage.equals(processedImage2)) {

					List<MachuImage> tempList = new LinkedList<MachuImage>();
					tempList.add(machuImage);
					machuImage
							.addEffect(new mvc.effects.impl.GrayscaleEffect());
					imageProcessor.processThumbnailImages(tempList);

					assertTrue(machuImage.getThumbnail() != image);
				}
			}
		}
	}
}
