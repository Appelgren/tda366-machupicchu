package mvc.library.impl;

import static org.junit.Assert.assertTrue;

import java.awt.Image;
import java.io.File;
import java.util.List;

import mvc.StringHolder;
import mvc.image.MachuImage;
import mvc.library.ILibrary;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestAllMethodsInLibrary {

	private ILibrary<MachuImage> library;
	private MachuImage img;
	private Image image;
	private String PATH_NAME;
	private File path;

	@Before
	public void init() throws Exception {
		PATH_NAME = StringHolder.blommabakgrund;
		path = new File(PATH_NAME);
		image = javax.imageio.ImageIO.read(path);
		img = new MachuImage(path, image);
		library = new Library<MachuImage>();

	}

	@After
	public void uninit() {
		PATH_NAME = null;
		path = null;
		image = null;
		img = null;
		library = null;

	}

	@Test
	public void testLibrary() {

		library.add(img);
		List<MachuImage> tempList = library.getImageList();
		library.remove(img);
		library.add(img);
		library.setSelectedImages(tempList);

		assertTrue(library.getImageList().get(0).getFile().equals(path)
				&& tempList.get(0).equals(library.getSelectedImages().get(0)));

	}

}
