package mvc.iogui;

import java.io.File;
import java.util.List;

import mvc.iogui.IFileChooser;

public class MockFileChooser implements IFileChooser {
        private File singleFile;
        private List<File> multipleFiles;
        private String fileExt;

        public MockFileChooser(File singleFile, List<File> multipleFiles,
                        String fileExt) {
                this.fileExt = fileExt;
                this.multipleFiles = multipleFiles;
                this.singleFile = singleFile;
        }

        @Override
        public File getFile() {
                return singleFile;
        }

        @Override
        public String getFileExtension() {
                return fileExt;
        }

        @Override
        public List<File> getFiles() {
                return multipleFiles;
        }

        @Override
        public boolean showFileChooser(List<String> afe) {
                return true;
        }

        public void setFileExt(String fileExt) {
                this.fileExt = fileExt;
        }

        public void setMultipleFiles(List<File> multipleFiles) {
                this.multipleFiles = multipleFiles;
        }

        public void setSingleFile(File singleFile) {
                this.singleFile = singleFile;
        }

}