package mvc.image;

import static org.junit.Assert.*;

import java.awt.Image;
import java.io.File;
import java.util.List;

import mvc.StringHolder;
import mvc.effects.IEffect;
import mvc.effects.impl.VerticalFlipEffect;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestingMachuImage {

	private MachuImage machuImage;
	private Image image;
	private String PATH_NAME;
	private File path;
	private VerticalFlipEffect verticalFX;

	@Before
	public void setUp() throws Exception {
		PATH_NAME = StringHolder.blommabakgrund;
		path = new File(PATH_NAME);
		image = javax.imageio.ImageIO.read(path);
		machuImage = new MachuImage(path, image);
		verticalFX = new VerticalFlipEffect();
	}

	@After
	public void tearDown() throws Exception {
		PATH_NAME = null;
		path = null;
		image = null;
		machuImage = null;
		verticalFX = null;
	}

	@Test
	public void testMachuImage() {

		// Adds an effect to the machuimage effectslist
		machuImage.addEffect(verticalFX);

		// Acquires the effect list of the machuimage and adds a effect to the
		// list.
		List<IEffect> tempList = machuImage.getEffectsList();
		tempList.add(new VerticalFlipEffect());

		// Checks so you can not manipulate the effectslist by using the
		// get-method and then adds elements to it.
		if (tempList.size() == 2 && machuImage.getEffectsList().size() == 1) {

			// Set the new list as effectlist in the machuimage
			machuImage.setEffectsList(tempList);

			// Checks that the list of effects set to the machuimage contains
			// the right amount of elements
			if (machuImage.getEffectsList().size() == tempList.size()) {

				// Sets the index of the current effect
				machuImage.setIndexOfCurrentEffect(1);

				// Checks that the right effect is set as selected
				if (machuImage.getIndexOfCurrentEffect() == 1) {

					// If all the above is correct then checks that the
					// following is right
					assertTrue(machuImage.getThumbnail().equals(image)
							&& machuImage.getFile().equals(path)
							&& machuImage.getEffectsList().get(0).equals(
									verticalFX));

				}
			}
		}
	}
}
