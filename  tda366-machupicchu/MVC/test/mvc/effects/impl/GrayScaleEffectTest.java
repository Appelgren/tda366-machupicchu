package mvc.effects.impl;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mvc.StringHolder;
import mvc.effects.IEffect;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/* This JUnit test will not pass but instead create one jpeg image
 * file called before.jpg and one called after.jpg to be checked manually.
 */
@SuppressWarnings("nls")
public class GrayScaleEffectTest {

	BufferedImage img;
	IEffect effect;

	@Before
	public void setUp() throws Exception {
		img = javax.imageio.ImageIO.read(new File(StringHolder.blommabakgrund));
		ImageIO.write(img, "jpg", new File(StringHolder.before));
	}

	@After
	public void tearDown() throws Exception {
		ImageIO.write(img, "jpg", new File(StringHolder.after));
		img = null;
		effect = null;
	}

	@Test
	public void testApplyEffect() throws IOException {
		effect = new GrayscaleEffect();

		//
		int rGBSampleBefore = img.getRGB(img.getWidth() / 2,
				img.getHeight() / 2);
		img = effect.applyEffect(img);
		int rGBSampleAfter = img
				.getRGB(img.getWidth() / 2, img.getHeight() / 2);

		assertTrue(rGBSampleBefore != rGBSampleAfter);

	}

}
