package mvc.effects.impl;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mvc.StringHolder;
import mvc.effects.IEffect;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/* This JUnit test will not pass but instead create one jpeg image
 * file called before.jpg and one called after.jpg to be checked manually.
 */
@SuppressWarnings("nls")
public class VerticalFlipEffectTest {

	BufferedImage img;
	IEffect effect;

	@Before
	public void setUp() throws Exception {
		img = javax.imageio.ImageIO.read(new File(StringHolder.blommabakgrund));
		ImageIO.write(img, "jpg", new File(StringHolder.before));
	}

	@After
	public void tearDown() throws Exception {
		ImageIO.write(img, "jpg", new File(StringHolder.after));
		img = null;
		effect = null;
	}

	@Test
	public void testApplyEffect() throws IOException {
		effect = new VerticalFlipEffect();

		// Gets a RGB color sample value of the top middle corner pixel
		int firstRGBtm = img.getRGB(img.getWidth() / 2, 0);
		// Gets a RGB color sample value of the bottom middle corner pixel
		int firstRGBbm = img.getRGB(img.getWidth() / 2, img.getHeight() - 1);

		img = effect.applyEffect(img);
		ImageIO.write(img, "jpg", new File("test/mvc/effects/after.jpg")); //$NON-NLS-2$

		// Gets another RGB color sample value of the top middle corner pixel
		int secondRGBtm = img.getRGB(img.getWidth() / 2, 0);
		// Gets another RGB color sample value of the bottom middle corner pixel
		int secondRGBbm = img.getRGB(img.getWidth() / 2, img.getHeight() - 1);

		img = effect.applyEffect(img);

		// Gets a last RGB color sample value of the top middle corner pixel
		int thirdRGBtm = img.getRGB(img.getWidth() / 2, 0);
		// Gets a last RGB color sample value of the bottom middle corner pixel
		int thirdRGBbm = img.getRGB(img.getWidth() / 2, img.getHeight() - 1);

		assertTrue(firstRGBtm == secondRGBbm && secondRGBbm == thirdRGBtm
				&& firstRGBbm == secondRGBtm && secondRGBtm == thirdRGBbm
				&& firstRGBtm != firstRGBbm);
	}

}
