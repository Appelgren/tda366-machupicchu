package mvc.effects.impl;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mvc.StringHolder;
import mvc.effects.IEffect;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/* This JUnit test will not pass but instead create one jpeg image
 * file called before.jpg and one called after.jpg to be checked manually.
 */
@SuppressWarnings("nls")
public class HorizontalFlipEffectTest {

	BufferedImage img;
	IEffect effect;

	@Before
	public void setUp() throws Exception {
		img = javax.imageio.ImageIO.read(new File(StringHolder.blommabakgrund));
		ImageIO.write(img, "jpg", new File(StringHolder.before));
	}

	@After
	public void tearDown() throws Exception {
		img = null;
		effect = null;
	}

	@Test
	public void testApplyEffect() throws IOException {
		effect = new HorizontalFlipEffect();

		// Gets a RGB color sample value of the middle right corner pixel
		int firstRGBmr = img.getRGB(img.getWidth() - 1, img.getHeight() / 2);
		// Gets a RGB color sample value of the middle left corner pixel
		int firstRGBml = img.getRGB(0, img.getHeight() / 2);

		img = effect.applyEffect(img);
		ImageIO.write(img, "jpg", new File(StringHolder.after)); //$NON-NLS-2$

		// Gets another RGB color sample value of the middle right corner pixel
		int secondRGBmr = img.getRGB(img.getWidth() - 1, img.getHeight() / 2);
		// Gets another RGB color sample value of the middle left corner pixel
		int secondRGBml = img.getRGB(0, img.getHeight() / 2);

		img = effect.applyEffect(img);

		// Gets a last RGB color sample value of the middle right corner pixel
		int thirdRGBmr = img.getRGB(img.getWidth() - 1, img.getHeight() / 2);
		// Gets a last RGB color sample value of the middle left corner pixel
		int thirdRGBml = img.getRGB(0, img.getHeight() / 2);

		assertTrue(firstRGBmr == secondRGBml && secondRGBml == thirdRGBmr
				&& firstRGBml == secondRGBmr && secondRGBmr == thirdRGBml
				&& firstRGBmr != firstRGBml);
	}

}
