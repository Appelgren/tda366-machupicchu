package mvc.effects.impl;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mvc.StringHolder;
import mvc.effects.IEffect;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/* This JUnit test will not pass but instead create one jpeg image
 * file called before.jpg and one called after.jpg to be checked manually.
 */
@SuppressWarnings("nls")
public class Rotate90RightEffectTest {

	BufferedImage img;
	IEffect effect;

	@Before
	public void setUp() throws Exception {
		img = javax.imageio.ImageIO.read(new File(StringHolder.blommabakgrund));
		ImageIO.write(img, "jpg", new File(StringHolder.before));
	}

	@After
	public void tearDown() throws Exception {
		ImageIO.write(img, "jpg", new File(StringHolder.after));
		img = null;
		effect = null;
	}

	@Test
	public void testApplyEffect() throws IOException {
		effect = new Rotate90RightEffect();

		int firstRGBtl = img.getRGB(0, 0);
		int firstRGBtr = img.getRGB(img.getWidth() - 1, 0);
		int firstRGBbl = img.getRGB(0, img.getHeight() - 1);
		int firstRGBbr = img.getRGB(img.getWidth() - 1, img.getHeight() - 1);

		img = effect.applyEffect(img);

		int secondRGBtl = img.getRGB(0, 0);
		int secondRGBtr = img.getRGB(img.getWidth() - 1, 0);
		int secondRGBbl = img.getRGB(0, img.getHeight() - 1);
		int secondRGBbr = img.getRGB(img.getWidth() - 1, img.getHeight() - 1);

		assertTrue(firstRGBtl == secondRGBtr && firstRGBtr == secondRGBbr
				&& firstRGBbr == secondRGBbl && firstRGBbl == secondRGBtl);
	}

}
