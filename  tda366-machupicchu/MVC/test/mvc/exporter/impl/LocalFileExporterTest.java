package mvc.exporter.impl;

import java.awt.Image;
import java.io.File;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import mvc.StringHolder;
import mvc.iogui.IFileChooser;
import mvc.iogui.MockFileChooser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("nls")
public class LocalFileExporterTest {
	private LocalFileExporter lfe;
	private IFileChooser fileChooser;
	private Image image;
	private File newFile;
	private File dir;
	private String newExt;
	private String filename;

	@Before
	public void setUp() throws Exception {
		newExt = "png";
		dir = new File(StringHolder.root);
		fileChooser = new MockFileChooser(dir, null, newExt);
		lfe = new LocalFileExporter(fileChooser);
		File f = new File(StringHolder.blommabakgrund);
		filename = f.getName();
		image = javax.imageio.ImageIO.read(f);
		newFile = new File(StringHolder.root
				+ filename.substring(0, filename.lastIndexOf(".") + 1) + newExt);
	}

	@After
	public void tearDown() {
		if (newFile.exists() && !newFile.delete()) {
			System.out.println("File could not be deleted");
		}
	}

	@Test
	public void test() throws Exception {
		if (lfe.prepareSave()) {
			lfe.saveImage(image, filename);
			Image image2 = javax.imageio.ImageIO.read(newFile);
			assertTrue("Not equal files", image2.getHeight(null) == image
					.getHeight(null));
		} else {
			fail("saveError");
		}
	}

}
