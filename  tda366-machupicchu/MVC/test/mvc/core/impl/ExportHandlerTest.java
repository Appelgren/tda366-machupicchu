package mvc.core.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mvc.StringHolder;
import mvc.exporter.impl.LocalFileExporter;
import mvc.image.MachuImage;
import mvc.imageProcessor.impl.ImageProcessor;
import mvc.iogui.MockFileChooser;
import mvc.utils.FileUtilitys;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("nls")
public class ExportHandlerTest {
	private LocalFileExporter lfe;
	private List<MachuImage> list;
	private File dir;
	private String newExt;

	@Before
	public void setUp() throws Exception {
		MachuImage im = new MachuImage(new File(StringHolder.after));
		MachuImage im2 = new MachuImage(new File(StringHolder.before));
		MachuImage im3 = new MachuImage(new File(StringHolder.blommabakgrund));
		list = new ArrayList<MachuImage>();
		list.add(im);
		list.add(im2);
		list.add(im3);

		newExt = "png";

		dir = new File(StringHolder.root + "machutest");
		if (dir.mkdir() || dir.exists()) {
			lfe = new LocalFileExporter(new MockFileChooser(dir, null, newExt));
		} else {
			fail();
		}
	}

	@Test
	public void test() throws Exception {
		ExportHandler eh = new ExportHandler(lfe, new ImageProcessor());
		eh.exportImages(list);
		for (MachuImage im : list) {
			String name = im.getFile().getName();
			name = name.substring(0,name.lastIndexOf(".") +1);
			File f = new File(dir.getAbsolutePath()+File.separator+name+newExt);
			if(!f.exists()){
				fail();
			}
		}
		FileUtilitys.removeFileDirectory(dir.getAbsolutePath());
		assertTrue(list.size()>0);
	}

	@After
	public void tearDown() throws Exception {
	}

}
