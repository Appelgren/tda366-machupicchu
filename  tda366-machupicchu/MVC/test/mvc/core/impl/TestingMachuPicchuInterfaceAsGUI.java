package mvc.core.impl;

import static org.junit.Assert.assertTrue;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mvc.StringHolder;
import mvc.core.IMachuPicchu;
import mvc.effects.IEffect;
import mvc.effects.impl.GrayscaleEffect;
import mvc.effects.impl.InvertEffect;
import mvc.image.MachuImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestingMachuPicchuInterfaceAsGUI {

	private List<MachuImage> imageList;
	private List<IEffect> effectsList;
	private IMachuPicchu machu;
	private MachuImage im2;

	@Before
	public void setUp() throws Exception {
		machu = MachuPicchu.getInstance();
		MachuImage im = new MachuImage(new File(StringHolder.after));
		im2 = new MachuImage(new File(StringHolder.before));
		imageList = new ArrayList<MachuImage>();
		imageList.add(im);

		effectsList = new ArrayList<IEffect>();
		effectsList.add(new GrayscaleEffect());
		effectsList.add(new InvertEffect());

	}

	@After
	public void tearDown() throws Exception {

		imageList = null;
		effectsList = null;
		machu = null;

	}

	@Test
	public void testMachuPicchuCore() {

		// Sets selected images.
		machu.setSelectedImages(imageList);

		// Adds second image to imageList
		imageList.add(im2);

		// Adds a new image to selected images
		machu.addToSelectedImages(im2);

		if (machu.getSelectedImages().size() == imageList.size()
				&& machu.getSelectedImages().get(0).equals(imageList.get(0))) {

			// Get list with selected images
			List<MachuImage> tempList = machu.getSelectedImages();

			tempList.get(0).addEffect(effectsList.get(0));
			Image processedImage = machu.processImage(imageList.get(0), 500,
					500);
			assertTrue(processedImage.getWidth(null) == 500);

		}
	}
}
