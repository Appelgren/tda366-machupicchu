package mvc.core.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mvc.StringHolder;
import mvc.effects.IEffect;
import mvc.effects.impl.GrayscaleEffect;
import mvc.effects.impl.InvertEffect;
import mvc.image.MachuImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class EffectsHandlerTest {
	private List<MachuImage> list;
	private List<IEffect> list2;

	@Before
	public void setUp() throws Exception {
		MachuImage im = new MachuImage(new File(StringHolder.after));
		MachuImage im2 = new MachuImage(new File(StringHolder.before));
		list = new ArrayList<MachuImage>();
		list.add(im);
		list.add(im2);
		list2 = new ArrayList<IEffect>();
		list2.add(new GrayscaleEffect());
		list2.add(new InvertEffect());
	}

	@Test
	public void test() throws Exception {
		EffectsHandler eh = new EffectsHandler();
		eh.addEffect(list, list2.get(0));
		eh.addEffect(list, list2.get(1));
		eh.removeLastEffect(list.get(0));
		for (IEffect ie : eh.getListWithEffects())
			System.out.println(ie.getClass().toString());
		assertTrue(list.get(1).getEffectsList().size() == 2);
	}

	@After
	public void tearDown() throws Exception {
		
	}

}
