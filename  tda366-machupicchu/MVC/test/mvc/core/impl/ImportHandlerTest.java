package mvc.core.impl;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import mvc.StringHolder;
import mvc.image.MachuImage;
import mvc.importer.IImporter;
import mvc.importer.impl.LocalFileImporter;
import mvc.iogui.MockFileChooser;
import mvc.utils.FileUtilitys;

import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class ImportHandlerTest {
	private IImporter lfi;
	private List<File> fileList;
	@Before
	public void setUp() throws Exception {
		File f1 = new File(StringHolder.blommabakgrund);
		File f2 = new File(StringHolder.after);
		File f3 = new File(StringHolder.before);
		fileList = new LinkedList<File>();
		fileList.add(f1);
		fileList.add(f2);
		fileList.add(f3);
		lfi = new LocalFileImporter(new MockFileChooser(null, fileList, null));
	}

	@Test
	public void test() throws Exception {
		ImportHandler ih = new ImportHandler(lfi);
		int i=0;
		for(MachuImage f : ih.getImages("testpath")){ //$NON-NLS-1$
			if(f.getFile().exists()){
				System.out.println(f.getFile() + " finns."); //$NON-NLS-1$
				i++;
			}
		}
		
		FileUtilitys.removeFileDirectory("testpath"); //$NON-NLS-1$
		assertTrue(i == fileList.size());
	}
	@After
	public void tearDown() throws Exception {
	}

}
