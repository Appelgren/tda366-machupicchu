package mvc.core.impl;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mvc.StringHolder;
import mvc.image.MachuImage;
import mvc.library.impl.Library;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LibraryHandlerTest {
	private LibraryHandler lh;
	private List<MachuImage> list;
	private List<MachuImage> list2;

	@Before
	public void setUp() throws Exception {
		lh = new LibraryHandler(new Library<MachuImage>());
		MachuImage im = new MachuImage(new File(StringHolder.after));
		MachuImage im2 = new MachuImage(new File(StringHolder.before));
		MachuImage im3 = new MachuImage(new File(StringHolder.blommabakgrund));
		list = new ArrayList<MachuImage>();
		list.add(im);
		list.add(im2);
		list.add(im3);

		list2 = new ArrayList<MachuImage>();
		list2.add(im);

	}

	@Test
	public void test() throws Exception {
		lh.addImages(list);
		lh.removeImages(list2);
		lh.addImage(list2.get(0));
		lh.setSelectedImages(list2);
		lh.removeImage(list.get(1));
		lh.addToSelectedImages(list.get(2));
		int i = 0;
		for (MachuImage im : lh.getImages()) {
			for (MachuImage im2 : lh.getSelectedImages()) {
				System.out.println("testar: " + im.getFile().getName() //$NON-NLS-1$
						+ " == " + im2.getFile().getName()); //$NON-NLS-1$
				if (im.getFile().getName().equals(im2.getFile().getName())) {
					i++;
					System.out.println("in"); //$NON-NLS-1$
				}
			}
		}
		System.out.println(i);
		assertTrue(i == 2);
	}

	@After
	public void tearDown() throws Exception {
	}

}
