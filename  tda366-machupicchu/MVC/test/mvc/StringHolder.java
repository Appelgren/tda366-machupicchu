package mvc;

@SuppressWarnings("nls")
public class StringHolder {
	public static String root = "/";
	public static String blommabakgrund = "test/mvc/effects/Blommabakgrund.jpg";
	public static String after = "test/mvc/effects/after.jpg";
	public static String before = "test/mvc/effects/before.jpg";
	public static final String COPIED_IMAGE1_PATH_IN_UTILS = "test/mvc/utils/tempdir/BlommabakgrundCopied.jpg";
	public static final String COPIED_IMAGE2_PATH_IN_UTILS = "test/mvc/utils/tempdir/afterCopied.jpg";
	public static final String TEMPDIR_IN_UTILS = "test/mvc/utils/tempdir";
}
